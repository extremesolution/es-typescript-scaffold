var inflected = require('inflected');
const promptRecursive = require('inquirer-recursive');
const path = require('path');
const devProdPath = __dirname.includes("node_modules") ? '../..' : '';

module.exports = (plop) => {
    const timeStamp = (() => {
        const format = (i) => {
            return parseInt(i, 10) < 10 ? '0' + i : i;
        };

        const date = new Date();
        return [
            date.getUTCFullYear(),
            format(date.getUTCMonth() + 1),
            format(date.getUTCDate()),
            format(date.getUTCHours()),
            format(date.getUTCMinutes()),
            format(date.getUTCSeconds())
        ].join('');
    })();

    plop.setPrompt('recursive', promptRecursive);

    plop.addHelper('pluralize', (input) => inflected.pluralize(input));
    plop.addHelper('timeStamp', () => timeStamp);

    // controller generator
    plop.setGenerator('scaffold', {
        description: 'application controller logic',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'what is the resource name?'
            },
            {
                type: 'confirm',
                name: 'api',
                message: 'Generate api template ?'
            },
            {
                type: 'recursive',
                message: 'Add attributes to the model',
                name: 'attributes',
                prompts: [
                    {
                        type: 'input',
                        name: 'attrName',
                        message: 'What is the name of attribute?'
                    },
                    {
                        type: 'list',
                        name: 'attrType',
                        message: 'What is the type of attribute?',
                        choices: ['string', 'number', 'boolean', 'Date']
                    }
                ]
            }
        ],
        actions: (data) => {
            let actions = [
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/src/app/controllers/{{properCase (pluralize name)}}Controller.ts'),
                    templateFile: 'templates/controller.ts'
                },
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/src/app/services/{{properCase (pluralize name)}}Service.ts'),
                    templateFile: 'templates/service.ts'
                },
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/src/app/repositories/{{properCase (pluralize name)}}Repository.ts'),
                    templateFile: 'templates/repository.ts'
                },
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/src/app/models/{{properCase name}}.ts'),
                    templateFile: 'templates/model.ts'
                },
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/test/integration/{{name}}.ts'),
                    templateFile: 'templates/test.ts'
                },
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/src/app/views/{{pluralize (camelCase name)}}/index.ejs'),
                    templateFile: 'templates/views/index.ejs'
                },
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/src/app/views/{{pluralize (camelCase name)}}/new.ejs'),
                    templateFile: 'templates/views/new.ejs'
                },
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/src/app/views/{{pluralize (camelCase name)}}/edit.ejs'),
                    templateFile: 'templates/views/edit.ejs'
                },
                {
                    type: 'add',
                    path: path.join(__dirname, devProdPath + '/src/db/migrations/{{timeStamp}}-{{pluralize (dashCase name)}}.js'),
                    templateFile: 'templates/migrations/migration.js'
                },
                {
                    type: 'append',
                    path: path.join(__dirname, devProdPath + '/src/app/types.ts'),
                    pattern: /Controller: Symbol\("Controller"\),/,
                    template: '    '
                        + '{{properCase (pluralize name)}}Service: Symbol("{{properCase (pluralize name)}}Service"),'
                        + '\n    '
                        + '{{properCase (pluralize name)}}Repository: Symbol("{{properCase (pluralize name)}}Repository"),'
                },
                {
                    type: 'append',
                    path: path.join(__dirname, devProdPath + '/src/app/controllers/index.ts'),
                    pattern: /\/\/ Controllers/,
                    template: 'export * from "./{{properCase (pluralize name)}}Controller";'
                },
                {
                    type: 'append',
                    path: path.join(__dirname, devProdPath + '/src/app/services/index.ts'),
                    pattern: /.*/,
                    template: 'export * from "./{{properCase (pluralize name)}}Service";'
                },
                {
                    type: 'append',
                    path: path.join(__dirname, devProdPath + '/src/app/repositories/index.ts'),
                    pattern: /.*/,
                    template: 'export * from "./{{properCase (pluralize name)}}Repository";'
                },
                {
                    type: 'append',
                    path: path.join(__dirname, devProdPath + '/src/inversify.config.ts'),
                    pattern: /export default container;/,
                    template: '\n'
                        + 'container.bind<{{properCase (pluralize name)}}Service>(TYPES.{{properCase (pluralize name)}}Service).to({{properCase (pluralize name)}}ServiceImpl);'
                        + '\n'
                        + 'container.bind<{{properCase (pluralize name)}}Repository>(TYPES.{{properCase (pluralize name)}}Repository).to({{properCase (pluralize name)}}RepositoryImplDb);'
                },
                {
                    type: 'modify',
                    path: path.join(__dirname, devProdPath + '/src/inversify.config.ts'),
                    pattern: /(} from "\.\/app\/repositories";)/,
                    template: '    '
                        + '{{properCase (pluralize name)}}Repository, {{properCase (pluralize name)}}RepositoryImplDb,'
                        + '\n'
                        + '$1'
                },
                {
                    type: 'modify',
                    path: path.join(__dirname, devProdPath + '/src/inversify.config.ts'),
                    pattern: /(} from "\.\/app\/services";)/,
                    template: '    '
                        + '{{properCase (pluralize name)}}Service, {{properCase (pluralize name)}}ServiceImpl,'
                        + '\n'
                        + '$1'
                }
            ];

            for (let attribute of data.attributes.reverse()) {
                actions.push({
                    type: 'append',
                    path: path.join(__dirname, devProdPath + '/src/app/models/{{properCase name}}.ts'),
                    pattern: /public id: number;/,
                    data: attribute,
                    template: '\n    '
                        + `@Column${attribute.attrType === 'Date' ? '(DataType.DATE)' : ''}`
                        + '\n    '
                        + 'public {{attrName}}: {{attrType}};'
                },
                    {
                        type: 'modify',
                        path: path.join(__dirname, devProdPath + '/src/app/controllers/{{properCase (pluralize name)}}Controller.ts'),
                        pattern: /((\s*)await [^\.]+\.save\(\);)/,
                        data: attribute,
                        template: '$2'
                            + (attribute.attrType === 'boolean'
                                ? '{{camelCase name}}.{{attrName}} = !!req.body.{{attrName}};'
                                : '{{camelCase name}}.{{attrName}} = req.body.{{attrName}};')
                            + '$1'
                    },
                    {
                        type: 'append',
                        path: path.join(__dirname, devProdPath + '/src/app/views/{{pluralize (camelCase name)}}/index.ejs'),
                        pattern: /(\n(\s*)<th>#<\/th>)/,
                        data: attribute,
                        template: '$2'
                            + '<th>{{attrName}}</th>'
                    },
                    {
                        type: 'append',
                        path: path.join(__dirname, devProdPath + '/src/app/views/{{pluralize (camelCase name)}}/index.ejs'),
                        pattern: /(\n(\s*)<td><%= [^\.]+\.id %><\/td>)/,
                        data: attribute,
                        template: '$2'
                            + `<td><%= {{name}}.{{attrName}}${attribute.attrType === "Date" ? '.toISOString().slice(0,10)' : ''} %></td>`
                    },
                    {
                        type: 'append',
                        path: path.join(__dirname, devProdPath + '/src/app/views/{{pluralize (camelCase name)}}/new.ejs'),
                        pattern: /(\n(\s*)<!--Start inputs here-->)/,
                        data: attribute,
                        templateFile: 'templates/views/fields/{{lowerCase attrType}}.ejs'
                    },
                    {
                        type: 'append',
                        path: path.join(__dirname, devProdPath + '/src/app/views/{{pluralize (camelCase name)}}/edit.ejs'),
                        pattern: /(\n(\s*)<!--Start inputs here-->)/,
                        data: { ...attribute, _isEdit: true },
                        templateFile: 'templates/views/fields/{{lowerCase attrType}}.ejs'
                    },
                    {
                        type: 'append',
                        path: path.join(__dirname, devProdPath + '/src/db/migrations/{{timeStamp}}-{{pluralize (dashCase name)}}.js'),
                        pattern: /(\n(\s*)\/\/ Start fields here)/,
                        data: attribute,
                        templateFile: 'templates/migrations/fields/{{lowerCase attrType}}.js'
                    }
                )

                if (attribute.attrType === 'boolean') {
                    actions.push(
                        {
                            type: 'modify',
                            path: path.join(__dirname, devProdPath + '/src/app/controllers/{{properCase (pluralize name)}}Controller.ts'),
                            pattern: /((\s*)const .+?DTO = [^\.]+\.build\(req\.body\);)/,
                            data: attribute,
                            template: '$2'
                                + 'req.body.{{attrName}} = !!req.body.{{attrName}};'
                                + '$1'
                        }
                    )
                }
            }

            if (data.api) {
                actions.push(
                    {
                        type: 'append',
                        path: path.join(__dirname, devProdPath + '/src/app/controllers/index.ts'),
                        pattern: /\/\/ API Controllers/,
                        template: 'export * from "./api/{{properCase (pluralize name)}}APIController";'
                    },
                    {
                        type: 'add',
                        path: path.join(__dirname, devProdPath + '/src/app/controllers/api/{{properCase (pluralize name)}}APIController.ts'),
                        templateFile: 'templates/apiController.ts'
                    }
                )
            }

            return actions;
        }
    });
};