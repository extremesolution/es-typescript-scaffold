import { Table, Column, Model, AutoIncrement, CreatedAt, UpdatedAt, DataType } from "sequelize-typescript";

@Table({ tableName: "{{pluralize name}}" })
export class {{properCase name}} extends Model<{{properCase name}}> {

    @AutoIncrement
    @Column({ primaryKey: true })
    public id: number;

    @CreatedAt
    createdAt: Date;

    @UpdatedAt
    updatedAt: Date;

}
