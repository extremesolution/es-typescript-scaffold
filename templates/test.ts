process.env.NODE_ENV = "test";

import * as chai from "chai";
import * as chaiHttp from "chai-http";
import app from "../../src/app";
import * as bCrypt from "bcrypt-nodejs";
import { JSDOM } from "jsdom";
import { {{properCase name}} } from "../../src/app/models/{{properCase name}}";
import * as Sequelize from "sequelize";

const should = chai.should();
const expect = chai.expect;
const parse = function (str) {
    const dom = new JSDOM(str);
    return dom.window.document;
};
const $ = function (document, selector) {
    return document.querySelector(selector);
};

chai.use(chaiHttp);

describe("{{properCase (pluralize name)}}", () => {

    before(() => {
    });

    after(() => {
        {{properCase name}}.destroy({ where: { id: { $gt: 1 } } });
    });

    describe("GET /{{pluralize name}}", () => {
        it("should open locations page", (done) => {
            const agent = chai.request.agent(app);
            agent.post("/signin")
                .send({
                    "email": "ahmedhany_5@hotmail.com",
                    "password": "asdasd"
                })
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.html;
                    res.redirects[0].should.include("{{pluralize name}}");
                    const req = await agent.get("/{{pluralize name}}");
                    req.res.should.be.html;
                    req.res.text.should.have.string('<h4 class="title">{{properCase (pluralize name)}}</h4>');
                    done();
                });
        });
    });

    describe("POST /{{pluralize name}}", () => {
        it("should signin user then create new {{name}}", (done) => {
            const agent = chai.request.agent(app);
            agent.post("/signin")
                .send({
                    "email": "ahmedhany_5@hotmail.com",
                    "password": "asdasd"
                })
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.html;
                    res.redirects[0].should.include("{{pluralize name}}");
                    const req = await agent.post("/{{pluralize name}}")
                        .send({ name: "yaw" });
                    req.res.should.be.html;
                    req.redirects[0].should.include("{{pluralize name}}");
                    const {{name}}Count = await {{properCase name}}.count();
                    expect({{name}}Count).to.be.greaterThan(0);
                    done();
                });
        });
    });

    describe("GET /{{pluralize name}}/:id/edit", () => {
        it("should open edit {{name}} page", (done) => {
            const agent = chai.request.agent(app);
            agent.post("/signin")
                .send({
                    "email": "ahmedhany_5@hotmail.com",
                    "password": "asdasd"
                })
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.html;
                    res.redirects[0].should.include("{{pluralize name}}");
                    const {{name}} = await {{properCase name}}.findOne();
                    agent.get(`/{{pluralize name}}/${ {{name}}.id}/edit`)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.should.be.html;
                            const nameField = $(parse(res.text), 'input[name="name"]');
                            nameField.should.exist;
                            nameField.defaultValue.should.be.equal({{name}}.name);
                            done();
                        });
                });
        });
    });

    describe("PATCH /{{pluralize name}}/:id", () => {
        it("should signin user then update {{name}}", (done) => {
            const agent = chai.request.agent(app);
            agent.post("/signin")
                .send({
                    "email": "ahmedhany_5@hotmail.com",
                    "password": "asdasd"
                })
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.html;
                    res.redirects[0].should.include("{{pluralize name}}");
                    const {{name}} = await {{properCase name}}.findOne();
                    const req = await agent.patch(`/{{pluralize name}}/${ {{name}}.id}`)
                        .send({ name: "yawUpdated" });
                    req.should.have.status(200);
                    req.res.should.be.html;
                    req.redirects[0].should.include("{{pluralize name}}");
                    const {{name}}Updated = await {{properCase name}}.findOne({ where: { name: "yawUpdated" } });
                    {{name}}Updated.should.exist;
                    done();
                });
        });
    });

    describe("DELETE /{{pluralize name}}/:id", () => {
        it("should delete specific {{name}}", (done) => {
            const agent = chai.request.agent(app);
            agent.post("/signin")
                .send({
                    "email": "ahmedhany_5@hotmail.com",
                    "password": "asdasd"
                })
                .end(async (err, res) => {
                    res.should.have.status(200);
                    res.should.be.html;
                    res.redirects[0].should.include("{{pluralize name}}");
                    const {{name}}ToBeDeleted = await {{properCase name}}.findOne({ where: { id: { $gt: 1 } } });
                    const {{name}}BeforeDelete = await {{properCase name}}.count();
                    const req = await agent.delete(`/{{pluralize name}}/${ {{name}}ToBeDeleted.id}`);
                    req.res.should.be.html;
                    const {{pluralize name}}Number = await {{properCase name}}.count();
                    {{pluralize name}}Number.should.to.be.lessThan({{name}}BeforeDelete);
                    done();
                });
        });
    });
});