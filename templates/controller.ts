import * as express from "express";
import container from "../../inversify.config";
import TYPES from "../types";
import { Authenticated } from "../decorators/Authenticated";
import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    interfaces,
    httpPatch
    } from "inversify-express-utils";
import { inject } from "inversify";
import { {{properCase name}} } from "../models/{{properCase name}}";
import { {{pluralize (properCase name)}}Service } from "../services/{{pluralize (properCase name)}}Service";

@controller("/{{pluralize name}}")
export class {{pluralize (properCase name)}}Controller implements interfaces.Controller {

    @inject(TYPES.{{pluralize (properCase name)}}Service)
    private {{pluralize name}}Service: {{pluralize (properCase name)}}Service;

    @Authenticated
    @httpGet("/")
    private async index(req: express.Request, res: express.Response, next: express.NextFunction) {
        const {{pluralize name}} = await this.{{pluralize name}}Service.get{{pluralize (properCase name)}}();
        res.render("{{pluralize name}}/index", { {{pluralize name}} });
    }

    @Authenticated
    @httpGet("/new")
    private async new(req: express.Request, res: express.Response, next: express.NextFunction) {
        res.render("{{pluralize name}}/new");
    }

    @Authenticated
    @httpPost("/")
    private async create(req: express.Request, res: express.Response, next: express.NextFunction) {
        try {
            const {{name}}DTO = {{properCase name}}.build(req.body);
            const {{name}} = this.{{pluralize name}}Service.create{{properCase name}}({{name}}DTO);
            res.flash("info", "{{properCase name}} has been created.");
            res.redirect("/{{pluralize name}}");
        } catch (err) {
            res.flash("error", err.message);
            res.redirect("back");
        }
    }

    @Authenticated
    @httpGet("/:id/edit")
    private async edit(req: express.Request, res: express.Response, next: express.NextFunction) {
        const {{name}} = await this.{{pluralize name}}Service.get{{properCase name}}(req.params.id);
        res.render("{{pluralize name}}/edit", { {{name}} });
    }

    @Authenticated
    @httpPatch("/:id")
    private async update(req: express.Request, res: express.Response, next: express.NextFunction) {
        try {
            const {{name}} = await this.{{pluralize name}}Service.get{{properCase name}}(req.params.id);
            await {{name}}.save();
            res.flash("info", "{{properCase name}} has been updated.");
            res.redirect("/{{pluralize name}}");
        } catch (err) {
            res.flash("error", err.message);
            res.redirect("back");
        }
    }

    @Authenticated
    @httpDelete("/:id")
    private async delete(req: express.Request, res: express.Response, next: express.NextFunction) {
        try {
            const {{name}} = this.{{pluralize name}}Service.delete{{properCase name}}(req.params.id);
            res.flash("info", "{{properCase name}} has been deleted.");
            res.redirect("/{{pluralize name}}");
        } catch (err) {
            res.flash("error", err.message);
            res.redirect("back");
        }
    }
}
