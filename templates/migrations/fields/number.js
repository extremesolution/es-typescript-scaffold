$2{{attrName}}: {
$2    type: Sequelize.INTEGER,
$2    defaultValueValue: 0,
$2    allowNull: false
$2},