import { injectable } from "inversify";
import { {{properCase name}} } from "../models/{{properCase name}}";

export interface {{pluralize (properCase name)}}Repository {
    findAll(): Promise<Array<{{properCase name}}>>;
    find(id: string): Promise<{{properCase name}}>;
    create({{name}}: {{properCase name}}): Promise<{{properCase name}}>;
    update({{name}}: {{properCase name}}): Promise<{{properCase name}}>;
    delete({{name}}: {{properCase name}}): Promise<void>;
}

@injectable()
export class {{pluralize (properCase name)}}RepositoryImplDb implements {{pluralize (properCase name)}}Repository {

    public async findAll(): Promise<Array<{{properCase name}}>> {
        return await {{properCase name}}.findAll();
    }

    public async find(id: string): Promise<{{properCase name}}> {
        return await {{properCase name}}.findById(id);
    }
    public async create({{name}}: {{properCase name}}): Promise<{{properCase name}}> {
        return await {{name}}.save();
    }

    public async update({{name}}: {{properCase name}}): Promise<{{properCase name}}> {
        return await {{name}}.save();
    }

    public async  delete({{name}}: {{properCase name}}): Promise<void> {
        return await {{name}}.destroy();
    }

}