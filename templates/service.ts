import { injectable, inject } from "inversify";
import { {{pluralize (properCase name)}}Repository } from "../repositories";
import TYPES from "../types";
import "reflect-metadata";
import { {{properCase name}} } from "../models/{{properCase name}}";

export interface {{pluralize (properCase name)}}Service {
    get{{pluralize (properCase name)}}(): Promise<Array<{{properCase name}}>>;
    create{{properCase name}}({{name}}: {{properCase name}}): Promise<{{properCase name}}>;
    update{{properCase name}}({{name}}: {{properCase name}}): Promise<{{properCase name}}>;
    get{{properCase name}}(id: string): Promise<{{properCase name}}>;
    delete{{properCase name}}(id: string): Promise<void>;
}

@injectable()
export class {{pluralize (properCase name)}}ServiceImpl implements {{pluralize (properCase name)}}Service {

    @inject(TYPES.{{pluralize (properCase name)}}Repository)
    private {{pluralize (camelCase name)}}RepositoryDb: {{pluralize (properCase name)}}Repository;

    public async get{{pluralize (properCase name)}}(): Promise<Array<{{properCase name}}>> {
        const {{properCase name}}Db: Array<{{properCase name}}> = await this.{{pluralize (camelCase name)}}RepositoryDb.findAll();
        return {{properCase name}}Db;
    }

    public async create{{properCase name}}({{name}}: {{properCase name}}): Promise<{{properCase name}}> {
        return await this.{{pluralize (camelCase name)}}RepositoryDb.create(await {{name}});
    }

    public async update{{properCase name}}({{name}}: {{properCase name}}): Promise<{{properCase name}}> {
        return await this.{{pluralize (camelCase name)}}RepositoryDb.update({{name}});
    }

    public async get{{properCase name}}(id: string): Promise<{{properCase name}}> {
        const {{name}} = await this.{{pluralize (camelCase name)}}RepositoryDb.find(id);
        return {{name}};
    }

    public async delete{{properCase name}}(id: string): Promise<void> {
        const {{name}} =  await this.get{{properCase name}}(id);
        return this.{{pluralize (camelCase name)}}RepositoryDb.delete({{name}});
    }

}