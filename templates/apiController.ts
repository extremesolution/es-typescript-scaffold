import * as express from "express";
import container from "../../../inversify.config";
import TYPES from "../../types";
import { Authenticated } from "../../decorators/Authenticated";
import {
    controller,
    httpGet,
    interfaces
} from "inversify-express-utils";
import { inject } from "inversify";
import { {{properCase name}} } from "../../models/{{properCase name}}";
import { {{pluralize (properCase name)}}Service } from "../../services";
import { promisify } from "util";

@controller("/api/{{pluralize name}}")
export class {{pluralize (properCase name)}}APIController implements interfaces.Controller {
    @inject(TYPES.{{pluralize (properCase name)}}Service)
    private {{pluralize name}}Service: {{pluralize (properCase name)}}Service;

    @httpGet("/")
    private async index(req: express.Request, res: express.Response, next: express.NextFunction) {
        const {{pluralize name}} = await this.{{pluralize name}}Service.get{{pluralize (properCase name)}}();
        // Issue in inversify util with sync and async
        const renderAsync = promisify(res.render).bind(res);
        return await renderAsync("api/{{pluralize name}}/index.json", { data: {{pluralize name}}, layout: false });
    }
}
